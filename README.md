# usb-console

Python script to easily connect to an external device that presents
serial links over a USB bridge.  If the external device provides
multiple serial links, all of them will be opened as panes in a single
tmux session, so all output can be seen easily.

The script scans for `/dev/ttyUSB*` and `/dev/ttyACM*` devices,
collects their position in the USB hierarchy, and groups them by
individual devices.

Target devices can be specified by *name* or *usb bus address*.

If no device is specified and serial links are present, `whiptail`
will be used to read the target in an interactive way.

# Requirements

To work correctly, the script needs external tools.  Be sure to
install them through by whatever means, e.g. by the package manager of
your distribution:

- tmux
- whiptail