#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import argparse
from subprocess import run, check_output, PIPE
from termcolor import colored
import os
from shutil import which

parser = argparse.ArgumentParser(
    prog = 'usb-console',
    description = 'Connect to serial ports of an USB device')
parser.add_argument('-l', '--list', action='store_true')
parser.add_argument('-n', '--name')
parser.add_argument('-p', '--phys')
args = parser.parse_args()

def error(msg):
    print(f'{os.path.basename(sys.argv[0])}: {colored("error:", "red")} {msg}')
    sys.exit(1)

def check_tool(tool):
    if which(tool) == None:
        error('External tool '+tool+' not installed. Please install through your distribution.')
    
def find_pattern(name, o1, o2):
    entries = []
    for path in run("find /sys/devices -name "+name+" | grep /tty/ | sort", shell=True, capture_output=True).stdout.decode().splitlines():
        dirs = path.split('/')
        entries.append(dict([('dev', dirs[-1]),
                             ('name', check_output('cat '+'/'. join(dirs[0:o1])+'/product', shell=True).decode().strip()),
                             ('phys', dirs[o2])]))
    return entries

check_tool('tmux')
check_tool('whiptail')

connections = find_pattern('ttyUSB*', -4, -5) + find_pattern('ttyACM*', -3, -4) 

# Collect all ports for a single physical device
dev_by_phys = {}
dev_by_name = {}
for d in connections:
    if d['phys'] in dev_by_phys:
        dev_by_phys[d['phys']]['ports'].append(d["dev"])
    else:
        dev_by_phys[d['phys']] = dict([('phys', d["phys"]),
                                       ('name', d["name"]),
                                       ('ports', [ d["dev"] ])])
    if d['name'] in dev_by_name:
        dev_by_name[d['name']]['ports'].append(d["dev"])
    else:
        dev_by_name[d['name']] = dict([('phys', d["phys"]),
                                       ('name', d["name"]),
                                       ('ports', [ d["dev"] ])])

if args.list:
    for d in connections:
        print(f"/dev/{d['dev']}\tname='{colored(d['name'],'green')}'\tphys={colored(d['phys'],'yellow')}")
    sys.exit(0)

ports = []
session_name = ""
if args.phys:
    if args.phys in dev_by_phys:
        session_name = args.phys
        ports = dev_by_phys[args.phys]["ports"]
    else:
        error("No device present at '"+args.phys+"'")
        
elif args.name:
    if args.name in dev_by_name:
        session_name = dev_by_phys[args.name]["phys"]
        ports = dev_by_phys[args.name]["ports"]
    else:
        error("No device with name '"+args.name+"' found")
else:
    # Build commandline for whiptail
    if len(dev_by_phys) > 1:
        pairs=[[phys, '{0} {1} ({2})'.format(phys, d['name'], str.join(", ",d['ports']))] \
               for phys, d in dev_by_phys.items()]
        options=["whiptail", "--notags", "--menu", "Select device", "20", "80", "5"] + \
            [item for sub in pairs for item in sub];
        ans = run(str.join(" ",map(lambda e: "'"+e+"'", options)), \
                  shell=True,check=True,stderr=PIPE).stderr.decode()
        session_name = ans
        ports = dev_by_phys[ans]["ports"]
    else:
        if len(dev_by_phys) > 0:
            phys = list(dev_by_phys.keys())[0]
            session_name = phys
            ports = dev_by_phys[phys]["ports"]
    
if len(ports)==0:
    error("No device found")

connected = 0
print(f'Connecting to {ports}')
run("tmux start-server", shell=True)
run("tmux new-session -d -s '"+session_name+"' -n '"+" ".join(ports)+"'", shell=True)
for p in ports:
    if connected>0:
        run("tmux splitw", shell=True)
    run("tmux selectp -t "+str(connected), shell=True)
    run("tmux send-keys 'kermit -l /dev/"+p+" -b 115200 -c' C-m", shell=True)
    connected = connected+1

run("tmux attach-session", shell=True)
