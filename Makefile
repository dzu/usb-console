.PHONY: install uninstall

install:
	cp usb-console.py /usr/local/bin

uninstall:
	rm /usr/local/bin/usb-console.py
